﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareGenerator : MonoBehaviour {

    public GameObject SquarePrefab;

    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            MakeRandomSquare();
        }
    }

    public GameObject MakeRandomSquare()
    {
        var squareObject = GameObject.Instantiate(SquarePrefab);
        var squareScript = squareObject.GetComponent<FourSquare>();
        squareScript.selectionCorners.GetComponent<Renderer>().enabled = false;
        int[] colors = new[]
        {
            Rnd.Get(3), Rnd.Get(3), Rnd.Get(3), Rnd.Get(3)
        };
        squareScript.SetColors(colors);
        return squareObject;
    }
}
