﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour {

    public const int SizeX = 16;
    public const int SizeY = 10;
    public const string EmptyCellObjectName = "Empty Cell Object";


    public const int LayerHand = 8;

    public const float animationTime = 0.5f;

    public GameObject cellBoxesRoot;
    public GameObject emptyCellPrefab;
    public Camera uiCamera;
    public Material cellMaterialRegular, cellMaterialHighlight;

    public GameObject[,] Board = new GameObject[SizeX, SizeY];
    public GameObject[,] CellBoxes = new GameObject[SizeX, SizeY];

    public List<GameObject> Hand = new List<GameObject>();
    public bool isBusy;
    public int hiX, hiY, hiHand, selectedHand;

    private SquareGenerator squareGen;

    // Use this for initialization
    void Start() {

        squareGen = GetComponent<SquareGenerator>();

        ForEach(CellBoxes, CreateCellBox);

        var first = squareGen.MakeRandomSquare();
        PlaceSquareAt(first, SizeX / 2, SizeY / 2, false);

        for (var n = 0; n < 12; n++)
        {
            AddSquareToHand();
        }
        ArrangeHandSquares();
        hiX = hiY = -1;
        hiHand = selectedHand = -1;
    }

    // Update is called once per frame
    void Update() {
        UpdateBoardHighlight();
        UpdateHandHighlight();
        if (Input.GetMouseButtonDown(0))
        {
            if (hiX >= 0 && hiY >= 0)
            {
                DoBoardClick(hiX, hiY);
            }
            else if (hiHand >= 0)
            {
                DoHandClick(hiHand);
            }
        }
    }

    public void ForEach<T>(T[,] array, Action<T, int, int, T[,]> obj)
    {
        for (var y = 0; y < SizeY; y++)
        {
            for (var x = 0; x < SizeX; x++)
            {
                obj(array[x, y], x, y, array);
            }
        }
    }

    private void UpdateHandHighlight()
    {
        if (!isBusy)
        {
            var ray = uiCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform != null)
                {
                    if (hit.transform.GetComponent<FourSquare>() != null)
                    {
                        DoHandHighlight(Hand.IndexOf(hit.transform.gameObject));
                        return;
                    }
                }
            }
        }
        DoHandHighlight(-1);
    }

    private void UpdateBoardHighlight()
    {
        if (!isBusy && selectedHand >= 0)
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform != null)
                {
                    if (hit.transform.gameObject.name == EmptyCellObjectName)
                    {
                        var x = (int)hit.transform.position.x;
                        var y = (int)hit.transform.position.z;
                        if (x >= 0 && y >= 0 && x < SizeX && y < SizeY)
                        {
                            DoBoardHighlight(x, y);
                            return;
                        }
                    }
                }
            }

        }
        DoBoardHighlight(-1, -1);
    }

    private bool CheckAndPlaceSquareAt(GameObject square, int x, int y, bool animate)
    {
        var deltaX = new[] { -1, 0, 1, 0 };
        var deltaY = new[] { 0, 1, 0, -1 };
        var oppDir = new[] { 2, 3, 0, 1 };

        var thisFour = square.GetComponent<FourSquare>();

        var nearCount = 0;
        for (var d = 0; d < 4; d++)
        {
            var xx = x + deltaX[d];
            var yy = y + deltaY[d];
            if (xx >= 0 && yy >= 0 && xx < SizeX && yy < SizeY)
            {
                var nearSquare = Board[xx, yy];
                if (nearSquare != null)
                {
                    var thatFour = nearSquare.GetComponent<FourSquare>();
                    if (thatFour != null && thatFour.SideColors != null && thatFour.SideColors.Length == 4)
                    {
                        if (thisFour.SideColors[d] != thatFour.SideColors[oppDir[d]]) return false;
                        nearCount++;
                    }
                }
            }
        }
        if (nearCount == 0) return false;
        PlaceSquareAt(square, x, y, animate);
        return true;
    }

    private void PlaceSquareAt(GameObject square, int x, int y, bool animate)
    {
        Board[x, y] = square;
        square.transform.position = new Vector3(x, 0.0f, y);
        if (animate)
        {
            StartCoroutine(DoSquarePlacementAnimation(square, CellBoxes[x, y]));
        }
        else
        {
            CellBoxes[x, y].GetComponent<Renderer>().enabled = false;
        }
    }

    private IEnumerator DoSquarePlacementAnimation(GameObject square, GameObject cellBox)
    {
        isBusy = true;
        square.SetActive(false);
        var startTime = Time.time;
        var rotationTime = 0.0f;
        var deltaTime = 0.0f;
        while (rotationTime < animationTime)
        {
            cellBox.transform.Rotate(new Vector3(deltaTime * 360.0f / animationTime, 0, 0));
            yield return new WaitForEndOfFrame();
            var newRotationTime = Time.time - startTime;
            deltaTime = newRotationTime - rotationTime;
            rotationTime = newRotationTime;
        }
        cellBox.transform.rotation = Quaternion.identity;

        square.GetComponent<FourSquare>().selectionCorners.GetComponent<Renderer>().enabled = false;
        square.transform.SetParent(transform);
        square.transform.position = cellBox.transform.position;
        square.transform.rotation = Quaternion.identity;
        square.SetActive(true);

        cellBox.GetComponent<Renderer>().enabled = false;
        yield return new WaitForSeconds(0.2f);
        isBusy = false;
    }

    private void CreateCellBox(GameObject cb, int x, int y, GameObject[,] boxes)
    {
        var cube = boxes[x, y] = GameObject.Instantiate(emptyCellPrefab);
        cube.transform.SetParent(cellBoxesRoot.transform);
        cube.transform.position = new Vector3(x, 0, y);
//        cube.GetComponent<MeshRenderer>().enabled = false;
        cube.name = EmptyCellObjectName;
    }

    private void AddSquareToHand()
    {
        var sq = squareGen.MakeRandomSquare();
        sq.layer = LayerHand;
        sq.transform.SetParent(uiCamera.transform);
        for(var n =0;n<sq.transform.childCount;n++)
        {
            var ch = sq.transform.GetChild(n);
            ch.gameObject.layer = LayerHand;
        }
        Hand.Add(sq);
    }

    private void ArrangeHandSquares()
    {
        var origin = uiCamera.ViewportToWorldPoint(new Vector3(0, 0, 10.0f));
        for (var n = 0; n < Hand.Count; n++)
        {
            var sq = Hand[n];
            sq.transform.position = origin + new Vector3(n * 1.01f + 1.0f, 1.0f, 0);
            sq.transform.rotation = Quaternion.identity;
            sq.transform.Rotate(90.0f, 90.0f, -90.0f);
        }
    }

    private void DoBoardHighlight(int x, int y)
    {
        if (x == hiX && y == hiY) return;

        if (hiX >= 0 && hiY >= 0)
        {
            CellBoxes[hiX, hiY].GetComponent<Renderer>().material = cellMaterialRegular;
        }
        hiX = x;
        hiY = y;
        if (hiX >= 0 && hiY >= 0)
        {
            CellBoxes[hiX, hiY].GetComponent<Renderer>().material = cellMaterialHighlight;
        }
    }

    private void DoHandHighlight(int num)
    {
        if (hiHand == num) return;

        if (hiHand >= 0 && hiHand < Hand.Count && hiHand != selectedHand)
        {
            Hand[hiHand].GetComponent<FourSquare>().selectionCorners.GetComponent<Renderer>().enabled = false;
        }
        hiHand = num;
        if (hiHand >= 0 && hiHand < Hand.Count && hiHand != selectedHand)
        {
            Hand[hiHand].GetComponent<FourSquare>().selectionCorners.GetComponent<Renderer>().enabled = true;
        }
    }

    private void DoHandSelect(int num)
    {
        if (selectedHand == num) return;

        if (selectedHand >= 0 && selectedHand < Hand.Count)
        {
            var render = Hand[selectedHand].GetComponent<FourSquare>().selectionCorners.GetComponent<Renderer>();
            render.material = cellMaterialRegular;
            render.enabled = selectedHand == hiHand;
        }
        selectedHand = num;
        if (selectedHand >= 0 && selectedHand < Hand.Count)
        {
            var render = Hand[selectedHand].GetComponent<FourSquare>().selectionCorners.GetComponent<Renderer>();
            render.material = cellMaterialHighlight;
            render.enabled = enabled;
        }
    }

    private void DoBoardClick(int hiX, int hiY)
    {
        if (hiX < 0 || hiY < 0) return;
        if (Board[hiX, hiY] != null) return;
        if (selectedHand < 0) return;

        var sq = Hand[selectedHand];

        if (CheckAndPlaceSquareAt(sq, hiX, hiY, true))
        {
            DoHandSelect(-1);
            DoHandHighlight(-1);
            Hand.Remove(sq);
            AddSquareToHand();
            ArrangeHandSquares();
        }
    }

    private void DoHandClick(int hiHand)
    {
        if (hiHand < 0) return;
        DoHandSelect(hiHand);
    }


}
