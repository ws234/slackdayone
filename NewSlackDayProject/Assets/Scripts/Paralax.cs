﻿using System.Collections;
using System.Collections.Generic;
using Tobii.Gaming;
using UnityEngine;

public class Paralax : MonoBehaviour {

    private Vector3 _headPose;
    private Camera _cam;
    private float left;
    private float right;
    private float top;
    private float bottom;
    private float near;
    private float far;
    private float horizontal = 0;
    private float vertical = 0;
    private float distance = 0;
    public float distanceScalar;
    // Use this for initialization
    void Start ()
    {
        
        TobiiAPI.SubscribeHeadPoseData();
        _cam = GetComponent<Camera>();
        Debug.Log(_cam.projectionMatrix);
        left = -_cam.nearClipPlane / _cam.projectionMatrix[0, 0];
        //left = Mathf.Tan(_cam.fieldOfView / 2f) * _cam.nearClipPlane;
        right = _cam.nearClipPlane / _cam.projectionMatrix[0, 0];
        //right = -Mathf.Tan(_cam.fieldOfView / 2f) * _cam.nearClipPlane;
        bottom = -_cam.nearClipPlane / _cam.projectionMatrix[1, 1];
        //bottom = ((left - right) * Screen.height / Screen.width) / 2;
        top = _cam.nearClipPlane / _cam.projectionMatrix[1, 1];
        //top = -((left - right) * Screen.height / Screen.width) / 2;
        near = _cam.nearClipPlane;
        far = _cam.farClipPlane;
    }
	
	// Update is called once per frame
	void LateUpdate ()
    {
        var headPose = TobiiAPI.GetHeadPose();
        _headPose = headPose.Position;
        Debug.Log("HeadPose: " + _headPose);

        Debug.Log("original: left: " + left + ", near: " + near + ", far: " + far);

        if (headPose.IsValid)
        {
            var newHorizontal = _headPose.x / TobiiAPI.GetDisplayInfo().DisplayWidthMm * (right - left);
            var newVertical = _headPose.y / TobiiAPI.GetDisplayInfo().DisplayHeightMm * (top - bottom);
            var newDistance = (1 - _headPose.z / distanceScalar) * 0.3f;
            Mathf.Clamp(newDistance, -0.3f, 1f);

            horizontal = Mathf.Lerp(horizontal, newHorizontal, 0.25f);
            vertical = Mathf.Lerp(vertical, newVertical, 0.25f);
            distance = Mathf.Lerp(distance, newDistance, 0.25f);

            var m = PerspectiveOffCenter(horizontal + left, horizontal + right, vertical + bottom, vertical + top, distance + near, far);
            _cam.projectionMatrix = m;
        }
        else
        {
            var m = PerspectiveOffCenter(left, right, bottom, top, near, far);
            _cam.projectionMatrix = m;
            Debug.Log(m);
        }
        Debug.Log("Horizontal: " + horizontal + ", Vertical: " + vertical + ", distance: " + distance);
    }

    private Matrix4x4 PerspectiveOffCenter(float left, float right, float bottom, float top, float near, float far)
    {
        float x = 2.0F * near / (right - left);
        float y = 2.0F * near / (top - bottom);
        float a = (right + left) / (right - left);
        float b = (top + bottom) / (top - bottom);
        float c = -(far + near) / (far - near);
        float d = -(2.0F * far * near) / (far - near);
        float e = -1.0F;
        Matrix4x4 m = new Matrix4x4();
        m[0, 0] = x;
        m[0, 1] = 0;
        m[0, 2] = a;
        m[0, 3] = 0;
        m[1, 0] = 0;
        m[1, 1] = y;
        m[1, 2] = b;
        m[1, 3] = 0;
        m[2, 0] = 0;
        m[2, 1] = 0;
        m[2, 2] = c;
        m[2, 3] = d;
        m[3, 0] = 0;
        m[3, 1] = 0;
        m[3, 2] = e;
        m[3, 3] = 0;
        return m;
    }
}
