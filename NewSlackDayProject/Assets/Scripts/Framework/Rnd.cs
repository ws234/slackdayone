﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rnd
{
    public static int Get(int max)
    {
        var v = (int)Random.Range(0.0f, (float)max);
        if (v < 0) v = 0;
        if (v >= max) v = max - 1;
        return v;
    }
}
