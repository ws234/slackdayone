﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FourSquare : MonoBehaviour {

    public GameObject TrianglePrefab;
    public Material[] ColorMaterials;
    public GameObject selectionCorners;

    public GameObject[] Sides;
    public int[] SideColors;

    public readonly Vector3[] SidePosition = new[]
    {
        new Vector3(-0.5f, 0.0f, -0.5f), new Vector3(-0.5f, 0, 0.5f),new Vector3(0.5f, 0, 0.5f),new Vector3(0.5f, 0, -0.5f)
    };
    public readonly Vector3[] SideRotation = new[]
    {
        new Vector3(), new Vector3(0, 90.0f, 0),new Vector3(0, 180.0f, 0),new Vector3(0,270.0f,0)
    };

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void SetColors(int[] colors)
    {
        if (colors.Length != 4) throw new ArgumentException();
        SideColors = colors;
        if (Sides != null && Sides.Length != 4)
        {
            foreach (var s in Sides) DestroyImmediate(s);
            Sides = null;
        }
        if (Sides == null)
        {
            Sides = new GameObject[4];
            for (var n = 0; n < 4; n++)
            {
                Sides[n] = GameObject.Instantiate(TrianglePrefab);
                Sides[n].transform.SetParent(transform);
            }
        }
        for (var n = 0; n < 4; n++)
        {
            Sides[n].GetComponent<Renderer>().material = ColorMaterials[SideColors[n]];
            Sides[n].transform.position = SidePosition[n];
            Sides[n].transform.rotation = Quaternion.identity;
            Sides[n].transform.Rotate(SideRotation[n]);
        }
    }
}
